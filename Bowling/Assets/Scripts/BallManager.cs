using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallManager : MonoBehaviour
{
    public List<Transform> boules = new List<Transform>();
    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < 4; i++)
        {
            boules.Add(GameObject.Find("/Boule " + i.ToString()).GetComponent<Transform>());
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void destruction()
    {
        for (int i = 0; i < boules.Count; i++)
        {
            if (boules[i].GetComponent<CheckPicked>().picked)
            {
                Destroy(boules[i].gameObject);
            }
        }
    }
}
