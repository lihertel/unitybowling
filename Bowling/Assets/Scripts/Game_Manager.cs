using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Game_Manager : MonoBehaviour
{
    private bool tour1 = true;
    public bool tour2 = false;

    public Text lancerTxt;
    public bool finTour = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // check si la boule a atteint la zone des quilles
        if(GameObject.Find("/Piste/Detector").GetComponent<Detector>().detected == true)
        {
            GameObject.Find("/Piste/Detector").GetComponent<Detector>().detected = false;
            StartCoroutine(attenteFinTour());
        }

        if(tour1 && finTour)
        {
            Debug.Log("ici");
            GetComponent<QuillesManager>().ecarterQuilles();        // on ecarte les quilles tomb�es
            GetComponent<BallManager>().destruction();                // on r�cup�re le score
            GetComponent<ScoreManager>().getScore();                // on r�cup�re le score
            finTour = false;                                        // on sort de l'�tat "fin du tour"
            tour1 = false;                                          // le tour 1 est fini
            tour2 = true;                                           // le tour 2 peut commencer
        }

        if (tour2 && finTour)
        {
            GetComponent<ScoreManager>().getScore();                // on r�cup�re le score
            // afficher que la partie est est fini
        }


        //gestion de la fin du 1er tour
        if (finTour)
        {
            GetComponent<QuillesManager>().ecarterQuilles();
            GetComponent<ScoreManager>().getScore();
            finTour = false;
            tour1 = false;
            tour2 = true;
        }

        if (tour1) { lancerTxt.text = "Lancer 1";}
        if (tour2) { lancerTxt.text = "Lancer 2";}
    }

    // fonction de chargement de la sc�ne
    public void restart()
    {
        Debug.Log("restart");
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    // fonction d'attente
    IEnumerator attenteFinTour()
    {
        Debug.Log("attente");
        yield return new WaitForSeconds(5);
        finTour = true;
    }
}
