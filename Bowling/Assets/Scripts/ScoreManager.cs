﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    public List<Transform> mesQuilles = new List<Transform>();

    public int score = 0;
    public Text scoreTxt;

    // Use this for initialization
    void Start()
    {
        for (int i=0; i<10; i++)
        {
            mesQuilles.Add(GameObject.Find("/Quille " + i.ToString()).GetComponent<Transform>());
        }
    }

    // Update is called once per frame
    void Update()
    {
        scoreTxt.text = "Score : " + score.ToString();
    }

    public void getScore()
    {
        for (int i = 0; i < 10; i++)
        {
            if(mesQuilles[i].GetComponent<CheckChute>().isFallen) { score++; }
        }
    }
}
