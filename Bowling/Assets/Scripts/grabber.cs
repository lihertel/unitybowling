using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using UnityEngine.XR.Interaction.Toolkit;
using Unity.XR.CoreUtils;

public class grabber : MonoBehaviour
{
    public XRNode controller;
    private InputDevice rightController;
    private RaycastHit hit;
    private GameObject grabbedObj = null;
    private bool attrape = false;
    private bool autorise = true;

    void Update()
    {
        rightController = InputDevices.GetDeviceAtXRNode(controller);
        rightController.TryGetFeatureValue(CommonUsages.triggerButton, out bool triggerValue);

        if(autorise)
        {
            Debug.Log("ici3");
          
            if (triggerValue)    // si l'utilisateur appuie sur la gachette
            {
                if (Physics.Raycast(transform.position, transform.forward, out hit, 10.0f))    // si l'on vise un objet et qu'il est proche
                {
                    if (hit.collider.gameObject.tag == "Boule" && !attrape)     // et que cet objet est la boule
                    {
                        Debug.Log("ici");
                        attrape = true;
                        grabbedObj = hit.collider.gameObject;    // on l'attrape
                        grabbedObj.transform.SetParent(transform);
                        grabbedObj.transform.localPosition = new Vector3(0f, 0f, 0f);
                        grabbedObj.GetComponent<Rigidbody>().isKinematic = true;
                        
                    }
                }
            }
            // sinon (si l'on appuie pas sur la gachette)
            else
            {
                if (attrape)
                {
                    Debug.Log("ici2");
                    grabbedObj.transform.SetParent(null); // si l'utilisateur n'appuie pas sur la gachette mais qu'on avait attrap� un objet, on le lache
                    attrape = false;
                    grabbedObj.GetComponent<Rigidbody>().isKinematic = false;
                    grabbedObj.GetComponent<Rigidbody>().AddForce((transform.up + 0.75f * transform.forward - 0.25f * transform.right) * 500);
                    grabbedObj.GetComponent<velocityManager>().lache = true;
                    StartCoroutine(Wait());



                }
            }
        }
        

    }

    IEnumerator Wait()
    {
        autorise = false;
        yield return new WaitForSeconds(5);
        autorise = true;
    }
}

