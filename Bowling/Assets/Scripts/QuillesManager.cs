using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class QuillesManager : MonoBehaviour
{
    public List<Transform> mesQuilles = new List<Transform>();
    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < 10; i++)
        {
            mesQuilles.Add(GameObject.Find("/Quille " + i.ToString()).GetComponent<Transform>());
        }

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ecarterQuilles()
    {
        Debug.Log("Ecarter Quilles");
        for(int i=0; i<10; i++)
        {
            if(mesQuilles[i].GetComponent<CheckChute>().isFallen)
            {
                mesQuilles[i].GetComponent<MeshRenderer>().enabled = false;
                mesQuilles[i].GetComponent<MeshCollider>().enabled = false;
            }
        }
    }

    public void repositionnerQuilles()
    {
        Debug.Log("Repositionner Quilles");
    }
}
