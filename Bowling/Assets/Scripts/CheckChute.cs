﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckChute : MonoBehaviour {
    private Transform myTf;
    public bool isFallen = false;

	// Use this for initialization
	void Start () {
        myTf = GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void Update () {

        if (Vector3.Angle(transform.up, Vector3.up) > 45)
        {
            isFallen = true;
        }
    }
}
